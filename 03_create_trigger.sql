USE [Students]
GO

/****** Object:  Trigger [SpecialityID_Constraint_on_Student]    Script Date: 21.12.2017 г. 17:38:46 ******/
DROP TRIGGER [dbo].[SpecialityID_Constraint_on_Student]
GO

/****** Object:  Trigger [dbo].[SpecialityID_Constraint_on_Student]    Script Date: 21.12.2017 г. 17:38:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[SpecialityID_Constraint_on_Student] ON [dbo].[Student]
AFTER INSERT,UPDATE
AS
BEGIN

	DECLARE @foundRows INT;

	IF UPDATE(SpecialityID)
	BEGIN
		SELECT @foundRows = COUNT(*) FROM Speciality INNER JOIN
			inserted i ON i.SpecialityID = Speciality.ID;

		IF (@foundRows = 0)
		BEGIN
			RAISERROR('Не може да добавите студент с избраната специалност', 11, 1);
		END;

	END;

END
GO

ALTER TABLE [dbo].[Student] ENABLE TRIGGER [SpecialityID_Constraint_on_Student]
GO


